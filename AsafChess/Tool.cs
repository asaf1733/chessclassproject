﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsafChess
{
    class Tool
    { 
        private string name;
        private bool oneMove;//זז הרבה או קצת
        private List<string> moves = new List<string>();
        private Image BToolImg;
        private Image WToolImg;
        string[] strmoves;
        public Tool(string n, bool hM, string mov, Image b, Image w)
        {
            this.name = n;
            this.oneMove = hM;
            this.WToolImg = w;
            this.BToolImg = b;
            strmoves = mov.Split(' ');
            for (int i = 0; i < strmoves.Length; i++)
            {
                moves.Add(strmoves[i]);
            }
        }
        public string GetName()
        {
            return this.name;
        }
        public string[] GetMoves()
        {
            return this.strmoves;
        }
        public bool GetOneMove()
        {
            return this.oneMove;
        }
    }
}
